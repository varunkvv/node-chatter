# node-chatter #

Library that handles automatic node discovery and removal. Very much WIP. Aims to become a full implementation of gossip protocol.

This implement two forms of discovery mechanism - SimpleChatter and ComplexChatter. SimpleChatter periodically sends requests for updates unconditionally. ComplexChatter "loses interest" after a certain number of tries - where there are no updates. Using these two together - both helps decrease network traffic as well ensures that all peers eventually have the same info.

### Methods Implemented ###

* Chatter.start / Chatter.stop 
* Chatter.getAlivePeers / Chatter.getDeadPeers
* Chatter.updateState - Used when a peer sends you data
* Chatter.prepareMessage - Used for sending data to a peer.

### What else needs to be done ###

Lots to be done but ...

- Keep a recent digest of updates and send only these
- Peers should be able to exchange more than just alive / dead hosts - they should also be able to store general metadata.
- Need to add tests
- Split code into bunch of classes
- Need to make Date UTC
- Benchmark against chatter intervals and number of nodes
- Use underscorejs instead of for loops.
- Use promises instead of callbacks
- Look into using immutable datastructures (like mori)